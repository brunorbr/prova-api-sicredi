package com.sicredi;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class RestrictionsTest {

    private static RequestSpecification requestSpec;

    @BeforeAll
    public static void setSpecifications(){
        requestSpec = new RequestSpecBuilder()
                .setBaseUri("http://localhost")
                .setPort(8080)
                .setBasePath("/api/v1/restricoes/")
                .build();
    }

    @ParameterizedTest
    @CsvSource({"26636670053", "03474348000", "53792190044",
            "60141150050", "40458615056"
    })
    public void NonRestrictedCPF_ReturnStatusCode204(String cpfInput){
        given().
                spec(requestSpec).
        when().
                get(cpfInput).
        then().
                assertThat().statusCode(204);

    }

    @ParameterizedTest
    @CsvSource({"97093236014", "60094146012", "84809766080",
            "62648716050", "26276298085"
    })
    public void RestrictedCPF_ReturnStatusCode200_ValidateMessage(String cpfInput){
        given().
                spec(requestSpec).
        when().
                get(cpfInput).
        then().
                assertThat().statusCode(200).
                and().
                assertThat().
                body("mensagem", equalTo("O CPF " + cpfInput + " possui restrição"));
    }
}
