package com.sicredi;

import com.sicredi.objects.SimulationObject;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.in;

public class SimulationsTest {

    private static RequestSpecification requestSpec;
    private static SimulationObject validData = new SimulationObject("Quentyn Martell",
            "63525369875", "quentyn@martell.io", "25000.00", 4, false);
    private static SimulationObject dataToBeDeletedOrUpdated = new SimulationObject("Torhen Stark", "52341274585",
            "torhen@stark.io", "36000.00", 8, true);
    private static SimulationObject invalidValue = new SimulationObject("Leyton Hightower", "02356874155", "leyton@hightower.io",
            "500.00", 3, false);
    private static SimulationObject invalidInstallments = new SimulationObject("Tytos Lannister", "45425266332", "tytos@lannister.io",
            "4500.00", 75, false);


    @BeforeAll
    public static void setSpecifications(){
        requestSpec = new RequestSpecBuilder()
                .setBaseUri("http://localhost")
                .setPort(8080)
                .setBasePath("/api/v1/simulacoes/")
                .setContentType(ContentType.JSON)
                .build();
    }

    @AfterAll
    public static void cleanUp(){
        String url = "http://localhost:8080/api/v1/simulacoes/";
        RestAssured.delete(url + Integer.toString(validData.getId()));
        RestAssured.delete(url + Integer.toString(dataToBeDeletedOrUpdated.getId()));
        RestAssured.delete(url + Integer.toString(invalidValue.getId()));
        RestAssured.delete(url + Integer.toString(invalidInstallments.getId()));
    }

    @Test
    public void ValidData_ReturnStatusCode201(){
        SimulationObject response =
        given().
                spec(requestSpec).
                body(validData).
        when().
                post().
        then().
                assertThat().statusCode(201).
                and().extract().as(SimulationObject.class);
        validData.setId(response.getId());
        Assertions.assertEquals(validData.toString(),response.toString());
    }

    @Test
    public void InvalidValue_ReturnStatusCode400(){
        SimulationObject response =
        given().
                spec(requestSpec).
                body(invalidValue).
        when().
                post().
        then().assertThat().statusCode(400).
                and().
                body("mensagem", equalTo("Valor inválido")).
                extract().as(SimulationObject.class);
        invalidValue.setId(response.getId());
    }

    @Test
    public void InvalidInstallments_ReturnStatusCode400(){
        SimulationObject response =
        given().
                spec(requestSpec).
                body(invalidInstallments).
        when().
                post().
        then().assertThat().statusCode(400).
                and().
                body("mensagem", equalTo("Número de parcelas inválido")).
                extract().as(SimulationObject.class);
        invalidInstallments.setId(response.getId());
    }

    @Test
    public void DeleteValidEntry_ReturnStatusCode(){
        SimulationObject response = given().spec(requestSpec).body(dataToBeDeletedOrUpdated)
                .when().post().then().extract().as(SimulationObject.class);
        dataToBeDeletedOrUpdated.setId(response.getId());
        given().
                spec(requestSpec).
        when().
                delete(Integer.toString(dataToBeDeletedOrUpdated.getId())).
        then().
                assertThat().statusCode(204);
    }

    @Test
    public void DeleteNonExistingEntry_ReturnStatusCode(){
        given().
                spec(requestSpec).
        when().
                delete("99999").
        then().
                assertThat().statusCode(404).
                and().body("mensagem", equalTo("Simulação não encontrada"));
    }

    @Test
    public void GetAllData_ReturnStatus200(){
        List list =
        given().
                spec(requestSpec).
        when().
                get().
        then().
                assertThat().statusCode(200).
                and().extract().as(List.class);
        Assertions.assertEquals(false, list.isEmpty());
    }

    @Test
    public void GetSimulationById_ReturnStatus200(){
        SimulationObject response = given().
                spec(requestSpec).
        when()
                .get(Integer.toString(validData.getId())).
        then().
                assertThat().statusCode(200).
                and().extract().as(SimulationObject.class);
        Assertions.assertEquals(validData.toString(), response.toString());
    }

    @Test
    public void GetSimulationByNonExistingId_ReturnStatus404(){
        given().
                spec(requestSpec).
        when()
                .get("99999999").
        then().
                assertThat().statusCode(404);
    }

    @Test
    public void UpdatedData_ReturnStatus200(){
        SimulationObject adding = given().spec(requestSpec).body(dataToBeDeletedOrUpdated)
                .when().post().then().extract().as(SimulationObject.class);
        dataToBeDeletedOrUpdated.setNome("Paxter Redwyne");
        dataToBeDeletedOrUpdated.setCpf("96385274111");
        dataToBeDeletedOrUpdated.setSeguro(true);
        SimulationObject response =
                given().
                        spec(requestSpec).
                        body(dataToBeDeletedOrUpdated).
                when().
                        put(Integer.toString(adding.getId())).
                then().
                        assertThat().statusCode(200).
                        and().extract().as(SimulationObject.class);
        dataToBeDeletedOrUpdated.setId(response.getId());
        Assertions.assertEquals(dataToBeDeletedOrUpdated.toString(), response.toString());


    }

    @Test
    public void RepeatedData_ReturnStatusCode409(){
        given().
                spec(requestSpec).
                body(validData).
        when().
                post().
        then().
                assertThat().statusCode(409).
                and().
                assertThat().body("mensagem", equalTo("CPF já existente"));
    }
}
