package com.sicredi.objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

public class SimulationObject {
    @JsonProperty
    private int id;
    @JsonProperty
    private String nome;
    @JsonProperty
    private String cpf;
    @JsonProperty
    private String email;
    @JsonProperty
    private BigDecimal valor;
    @JsonProperty
    private int parcelas;
    @JsonProperty
    private boolean seguro;

    public SimulationObject(String nome, String cpf,
                            String email, String valor,
                            int parcelas, boolean seguro){
        this.cpf = cpf;
        this.nome = nome;
        this.email = email;
        this.valor = new BigDecimal(valor);
        this.parcelas = parcelas;
        this.seguro = seguro;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public void setNome(String novoNome){
        this.nome = novoNome;
    }

    public void setCpf(String cpf){
        this.cpf = cpf;
    }

    public void setSeguro(boolean seguro){
        this.seguro = seguro;
    }

    @Override
    public String toString() {
        return "SimulationObject{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                ", email='" + email + '\'' +
                ", valor=" + valor +
                ", parcelas=" + parcelas +
                ", seguro=" + seguro +
                '}';
    }
}
