# Teste Técnico
Prova técnica de teste de API para o Sicredi.

## Pré-Requisitos:

* JDK8+;
* Maven 3.6.3;
* A API de Simulação e Restrição deve estar em execução.

## Como Executar

Após clonar o repositório, em um terminal, execute o comando:
```bash
mvn surefire:test
```

Os resultados de execução são exibidos no terminal.
